In the beginning, this was https://forum.riot-os.org/t/coap-remote-shell/3340/5

Being implemented in https://github.com/RIOT-OS/RIOT/pull/19289

2023-03-03 pitching to core-href folks, as it's-stdio-but-also-blockwise-alternative. Carsten: Could make a nice T2TRG document.

Structure for here:

* stdiout streaming format
  * Means of viewing an indefinite-length resource of which not necessarily all history is around
  * Observation view defined (for clarity, might need explicit fetch body)
  * In this, competing with block-wise, and related to STP
  * Has inner content-type (typically text/plain, or ... text/plain;term=vt100?)
  * The trouble of restarting

* options for stdin
  * plain line-wise (just registering rt?)
  * streaming in opposite direction? or just streaming with reverse URI?

* extensions and how they'd conflict with the streaming model
  * packed cbor / pointing into .hex file?
  * what does this mean for the inner content format, esp. when one starts doing dcaf over it?
  * what does it mean for the indices?
    * are indices maybe better done item-wise and not byte-wise? might the whole concept come in two forms, one for byte-wise, and one for record-wise (whatever the records are)? (does text consist of line records?)
      * we can show visually that we missed some items instead of having missed some bytes, but can we somehow still have growing strings? or does every string need to be terminated as soon as it's first seen?
        (like, we send in observations `1, "hel"`, `1, "hello"`, `2, " world"`, how does the receiver know it missed the updated item 1 if the 2nd notification is lost?
  * same exercise but using CoAP options rather than payloads? nice property: Accept/Content-Format could be reused (b/c while we can't really lie if all we do is in the body, other options can alter Accept/Content-Format just like Block2 said it relates to the body not the payload).
  * thought experiment: If we did classical block-wise using this, how bad would it be? (early guess: less control unless we introduce client-configured size; more overhead due to less efficient encoding; need Content-Format incompatible with opportunistic use; no means of telling that this-is-the-end-of-the-representation, only as-of-now)
